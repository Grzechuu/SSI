
def is_atr_s(atr: str):
    return atr == 's'


def load_file(sample_input: str, types: str):
    samples = []
    atr_names = []
    is_atr_symb = []
    with open(sample_input, 'r') as sample_content, open(types, 'r') as type_content:
        sample_lines = sample_content.readlines()
        type_lines = type_content.readlines()

        for line in sample_lines:
            samples.append(list(filter(None, line.replace(' ', '\t').strip().split('\t'))))
        for line in type_lines:
            name, type = list(filter(None, line.replace(' ', '\t').strip().split('\t')))
            atr_names.append(name)
            is_atr_symb.append(is_atr_s(type))

    return samples, atr_names, is_atr_symb


if __name__ == '__main__':
    print(load_file('./SSI_1/australian.txt', './SSI_1/australian-type.txt'))
