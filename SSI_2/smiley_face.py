import numpy as np
from math import pi

def ellipsis_equation(x_center: int, y_center: int, quantity = 100, x_radius = 2, y_radius = 2):
    values = np.linspace(0, 2 * pi, quantity)
    x_values = x_center + x_radius * np.cos(values)
    y_values = y_center + y_radius * np.sin(values)

    return x_values, y_values

def sinus_values(start: float, end: float, quantity = 100):
    return np.linspace(start, end, quantity), np.linspace(-1, 1, quantity)

