from SSI_1.lab01 import load_file
from SSI_2.smiley_face import ellipsis_equation, sinus_values
from math import pi

import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

# CREATING SMILEY FACE
def smiley_plot():
    x_ellipsis, y_ellipsis = ellipsis_equation(0, 0)
    sin_values, x_values = sinus_values(pi, 2 * pi)
    plt.plot(y_ellipsis, x_ellipsis, color="red", label="lamana")

    plt.plot(x_values, np.sin(sin_values), color="yellow", label="sinus")

    point_x = [-1, 0, 1]
    point_y = [1, 0, 1]
    plt.scatter(point_x, point_y, marker="D", label="punkty")
    plt.show()


def conver_to_numeric(data_frame):
    for i in data_frame.columns:
        data_frame[i] = pd.to_numeric(data_frame[i])


def subplots():
    data, atr_names, _ = load_file("./iris.txt", "./iris-type.txt")
    data_frame = pd.DataFrame(data, columns=atr_names)
    conver_to_numeric(data_frame)
    values = []
    values.append(data_frame[:][data_frame[atr_names[-1]] == 1])
    values.append(data_frame[:][data_frame[atr_names[-1]] == 2])
    values.append(data_frame[:][data_frame[atr_names[-1]] == 3])
    plt.figure()
    plt.subplots(figsize=(10, 8))
    plt.subplot(2, 2, 1)
    # plt.xticks(rotation=45)
    for i in range(3):
        sns.scatterplot(
            x=values[i][atr_names[2]],
            y=values[i][atr_names[3]],
            hue=data_frame[atr_names[-1]],
            legend=None,
        )
    plt.subplot(2, 2, 2)
    for i in range(3):
        sns.scatterplot(
            x=values[i][atr_names[1]],
            y=values[i][atr_names[3]],
            hue=data_frame[atr_names[-1]],
            legend=None,
        )
    plt.subplot(2, 2, 3)
    for i in range(3):
        sns.scatterplot(
            x=values[i][atr_names[0]],
            y=values[i][atr_names[3]],
            hue=data_frame[atr_names[-1]],
            legend=None,
        )

    plt.subplot(2, 2, 4)
    for i in range(3):
        sns.scatterplot(
            x=values[i][atr_names[1]],
            y=values[i][atr_names[2]],
            hue=data_frame[atr_names[-1]],
            legend=None,
        )

    plt.figlegend(loc="lower right", labels=["Setosa", "Versicolour", "Virginica"])
    plt.show()


if __name__ == "__main__":
    smiley_plot()
    subplots()
