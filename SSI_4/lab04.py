import numpy as np
import random
import matplotlib.pyplot as plt

dispersion = 10
step = 1.1
iterations = 100
bound = [0, 100]


def fun(x):
    return np.sin((x / 10)) * np.sin((x / 200))


x_axis = np.linspace(*bound, 300)


def algorithm(dispersion: int, step: float, iterations=100):
    plt.ion()
    x = random.choice(np.arange(*bound, 1))
    y = fun(x)
    results = []
    for i in range(iterations):
        xpot = x + random.choice(np.arange(dispersion * -1, dispersion, 1))
        xpot = np.clip([xpot], *bound)[0]
        ypot = fun(xpot)
        if ypot >= y:
            x = xpot
            y = ypot
            dispersion = dispersion * step
        elif ypot < y:
            dispersion = dispersion / step
        results.append({"x": x, "y": y, "itteration": i, "dispersion": dispersion})

        plt.cla()
        plt.scatter(x, y, s=200, c='red')
        plt.scatter(xpot, ypot, s=200, c='blue')
        plt.text(0, 0.35, 'Itteration=%.0i' % i)
        plt.title('funkcja przystosowania sin(x/10) * sin(x/200)')
        plt.plot(x_axis, fun(x_axis))
        plt.pause(0.05)
        plt.show()
    plt.show(block=True)

if __name__ == '__main__':
    algorithm(dispersion, step, iterations)
