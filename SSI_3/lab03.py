# M - number of centroids to pick from dataset
import random

import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

from collections import defaultdict
from SSI_1.lab01 import load_file


def convert_to_numeric(array: [str]) -> [float]:
    return list(map(float, array))


def compute_center_distance(point, centroid):
    return np.sqrt(np.sum(pow(np.array(point) - np.array(centroid), 2)))


def pick_centroids(m=4):
    random_centers = []
    for i in range(0, m):
        centers = random.choice(data[0])
        random_centers.append(convert_to_numeric(centers))
    return random_centers


# min() return the value in the first value in sorted.
# key designate the way to
# sort the values. key=d.get means the list will be sorted by values of the dictionary.


def minimal_distance_index(distance):
    # index = min(distance, key=lambda i: distance[i])
    index = min(distance, key=distance.get)
    return index


def new_centroid(points):
    if len(points) > 1:
        t = np.mean(points, axis=0)
        return t
    return points


def k_means(
    sample_points,
    centroids,
    iterations: int = 100,
):
    for iteration in range(iterations):
        cluster = defaultdict(list)

        for point_index, point in enumerate(sample_points):
            center_distance = {}

            for centroid_index, centroid in enumerate(centroids):
                center_distance[centroid_index] = compute_center_distance(
                    convert_to_numeric(point), centroid
                )
            closest_centroid_idx = minimal_distance_index(center_distance)
            cluster[closest_centroid_idx].append(convert_to_numeric(point))

        for idx, cluster_points in cluster.items():
            centroids[idx] = new_centroid(cluster_points)

    return cluster, centroids


def draw_plot(cluster, centroids):
    concatenate = []
    concatenate_with_cluster_name = []
    centroids_with_cluster_name = []
    for x in cluster.values():
        concatenate.extend(x)
    for key, values in cluster.items():
        for points in values:
            concatenate_with_cluster_name.append(
                {"x": points[0], "y": points[1], "cluster": key}
            )

    for idx, coords in enumerate(centroids):
        centroids_with_cluster_name.append(
            {"x": coords[0], "y": coords[1], "cluster": idx}
        )

    df_points = pd.DataFrame(concatenate_with_cluster_name)
    df_center = pd.DataFrame(centroids_with_cluster_name)

    sns.scatterplot(data=df_points, x="x", y="y", hue="cluster")
    plt.legend(title="Cluster")
    sns.scatterplot(
        data=df_center, x="x", y="y", hue="cluster", marker="+", legend=None, s=100
    )
    plt.show()


if __name__ == "__main__":
    data = load_file("./spirala.txt", "./spirala-type.txt")

    centroids = pick_centroids()

    cluster, centers = k_means(data[0], centroids)

    draw_plot(cluster, centers)
