import numpy as np
import matplotlib.pyplot as plt
from SSI_6.lab06 import from_file_to_matrix
from matplotlib.colors import ListedColormap



template1 = np.array(
    [
        [0, 0, 0, 1],
        [0, 0, 1, 1],
        [0, 1, 0, 1],
        [0, 0, 0, 1],
        [0, 0, 0, 1],
    ]
)

template2 = np.array(
    [
        [0, 1, 1, 1],
        [1, 0, 0, 1],
        [0, 0, 1, 0],
        [0, 1, 0, 0],
        [1, 1, 1, 1],
    ]
)

template3 = np.array(
    [
        [1, 1, 1, 0],
        [0, 0, 0, 1],
        [1, 1, 1, 1],
        [0, 0, 0, 1],
        [1, 1, 1, 0],
    ]
)

# templates = [template1, template2, template3]

templates = from_file_to_matrix('templates.txt', 5, 4)

given_template = from_file_to_matrix('input.txt', 5, 4)

# //TODO LOADING FILES WITH TEMPLATE

def greedy_algorithm(given_template):

    score = []
    for index, template in enumerate(templates):
        result = []
        result2 = []
        test_bitmap_ones_positions = search_for_value(given_template)
        template_bitmap_ones_positions = search_for_value(template)

        for point in test_bitmap_ones_positions:
            result.append(pick_closest(point, template_bitmap_ones_positions))

        for point in template_bitmap_ones_positions:
            result2.append(pick_closest(point, test_bitmap_ones_positions))
        score.append((-1 * (sum(result) + sum(result2)), template))
    return max(score)


def point_distance(a, b):
    return np.sqrt(np.sum(pow(np.array(a) - np.array(b), 2)))


def pick_closest(point, template):
    result = []
    for compare_item in template:
        if compare_item[0] == point[0] or compare_item[1] == point[1]:
            result.append(point_distance(point, compare_item))
    return min(result)


def search_for_value(given_template, value=1):
    result = []
    for row_index, row in enumerate(given_template):
        for column_index, column in enumerate(given_template[row_index]):
            item = given_template[row_index][column_index]
            if item == value:
                result.append((row_index, column_index))
    return result


# def search_for_value(given_template, value=1):
#     # return np.argwhere(given_template == value)
#     result = defaultdict(list)
#     for row_index, row in enumerate(given_template):
#         for column_index, column in enumerate(given_template[row_index]):
#             item = given_template[row_index][column_index]
#             if item == value:
#                 result[row_index].append(((row_index, column_index)))
#                 # result.append((row_index, column_index))
#     return result


if __name__ == "__main__":
    cmap = ListedColormap(["k", "w"])
    result = greedy_algorithm(given_template)
    fig, axs = plt.subplots(2, 3, figsize=(15, 8), sharex=True, sharey=True)
    (ax1, ax2, ax3), (ax4, ax5, ax6) = axs
    fig.suptitle("Algorytm zachłanny")
    ax1.matshow(given_template, cmap=cmap)
    ax1.set_aspect("auto")
    ax1.set_title("Wejscie")

    ax2.matshow(result[1], cmap=cmap)
    ax2.set_aspect("auto")
    ax2.set_title("Wyjście")

    fig.delaxes(ax3)

    ax4.matshow(template1, cmap=cmap)
    ax4.set_aspect("auto")
    ax4.set_title("template 1")

    ax5.matshow(template2, cmap=cmap)
    ax5.set_aspect("auto")
    ax5.set_title("template 2")

    ax6.matshow(template3, cmap=cmap)
    ax6.set_aspect("auto")
    ax6.set_title("template 3")
    plt.show()
