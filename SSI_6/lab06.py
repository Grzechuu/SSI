import numpy as np
import matplotlib.pyplot as plt
import math

test_data = [
    [
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
    ],
    [
        [1, 1, 0, 0, 1],
        [0, 1, 0, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 0, 1, 0],
        [1, 1, 0, 0, 1],
    ],
    [
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
    ],
    [
        [0, 1, 1, 1, 1],
        [1, 0, 1, 1, 1],
        [1, 0, 1, 1, 1],
        [1, 0, 1, 1, 1],
        [1, 0, 1, 1, 1],
    ],
]


template1 = np.array(
    [
        [1, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
    ]
)

template2 = np.array(
    [
        [1, 0, 0, 0, 1],
        [0, 1, 0, 1, 0],
        [0, 0, 1, 0, 0],
        [0, 1, 0, 1, 0],
        [1, 0, 0, 0, 1],
    ]
)

template3 = np.array(
    [
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [1, 1, 1, 1, 1],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
    ]
)

train_data = [template1, template2, template3]


class Hopfield:
    def __init__(self, size):
        neurons_count = size * size
        self.neurons_count = neurons_count
        self.weights = np.zeros((neurons_count, neurons_count))

    def hebb(self, test_list_flattened):
        return np.sum(np.multiply(self.weights, test_list_flattened), axis=1)

    def calculate_weights(self, x):
        weights = np.outer(x, x)
        # if i=j than 0 so diagonal must be 0
        np.fill_diagonal(weights, 0)
        # why np.fill_diagonal(np.outer(x,x), 0) IS THROWING ERROR
        return weights

    def predict(self, test_list):
        test_list = np.asarray(test_list)
        test_list = np.where(test_list == 0, -1, test_list)
        row, col = test_list.shape
        pred = np.where(
            self.hebb(np.reshape(test_list, self.neurons_count)) >= 0, 1, -1
        )
        return np.reshape(pred, (row, col))

    def check(self, test_samples):

        test_matrix = np.asarray(test_samples)
        test_matrix = np.where(test_matrix == 0, -1, test_matrix)
        for item in test_matrix:
            self.weights += self.calculate_weights(item) / self.neurons_count


if __name__ == "__main__":
    k = Hopfield(5)
    k.check(train_data)
    test = test_data[1]
    for _ in range(2):
        predict = k.predict(test)
        predict[predict == -1] = 0
        fig, (ax1, ax2) = plt.subplots(2, 1)
        ax1.imshow(test, cmap="binary")
        ax2.imshow(predict, cmap="binary")
        test = predict
    plt.show()
